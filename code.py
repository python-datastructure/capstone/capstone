
# 1. Write a python statements that will satisfy the following situations and requirements:

	# a. Create a "numbers" list that will perform the following operations:
		# items that should be in the list: 5,11,17,19,25,29,30,30,32,46,90
numbers = [5,11,17,19,25,29,30,30,32,46,90]
		# a.1. Find the total number of items in the list.
print(len(numbers))

		# a.2. Find how many times the item "30" occurs in the list
print(numbers.count(30))
		# a.3. Remove the item 17 from the numbers list.
numbers.remove(17)
print(numbers)

	# b. Create a SumEvenOdd() function to find the sum of all "even" and "odd" items in the "numbers" list.

def SumEvenOdd():
	even=0
	odd=0
	for i in numbers:
		if i%2 ==0:
			even +=i
			
		else:
			odd +=i
	print(f"Sum of even numbers: {even}")
	print(f"Sum of odd numbers: {odd}")
			

print("Sum of all even and odd numbers in the list:")
SumEvenOdd()			




	# c. Marco is a programmer working on a project related with stack data structure. He is currently working with the functionality of storing visitors' name by implementing PUSH and POP method. But both of the functionalities are producing incorrect result. Help Marco on debugging his code by writing the correct code for the specific line number:

visitors = []

def PUSH(name):
	visitors.append(name) 
	top = len(visitors)-1

def POP():
	if len(visitors) == 0:
		return "Sorry! No Visitor to delete!"
	else:
		val = visitors.pop() 
		# if len(visitors) == 1: 
		# 	top == None 
		# else:
		# 	top == len(visitors) - 1
		return val



print(f"\nCurrent content of visitors: {visitors}")
print(f"Current length of the visitors: {len(visitors)}")
print("Invoking the POP method:")

print(POP())
PUSH("John")

print(f"\nUpdated visitors list after using PUSH('John') Method: {visitors}")
print(f"Current length of visitors list after using PUSH('John') Method: {len(visitors)}")
print("Invoking the POP method:")
print(POP())
print("Invoking the POP method again:")
print(POP())
print(f"Updated visitors list after using POP() Method: {visitors}")

	# d. Miah is a python programmer working on queue data structures. She implemented the ENQUEUE and DEQUEUE methods for managing the data base on the REQUESTNO. But both of the functionalities are not producing the desired results. Help Miah on debugging his code by writing the correct code for the specific line number:

requestsNo = []

def enqueue(item):
	requestsNo.append(item) 
	if len(requestsNo) == 0: 
		front == rear == 0
	else:
		rear = len(requestsNo)-1

def dequeue():
	if len(requestsNo) == 0:
		print("Underflow") 
	else:
		val = requestsNo.pop()
	# if len(requestsNo) == 0: 
	# 	front = rear = None 
		return val

print(f"\nCurrent content of requestsNo: {requestsNo}")
print(f"Current length of the requestsNo: {len(requestsNo)}")
print("Invoking the dequeue() method: ")
print(dequeue())

enqueue("RQN001")
enqueue("RQN002")

print(f"\nCurrent content of requestsNo after enqueue() method: {requestsNo}")
print(f"Current length of the requestsNo after enqueue()method : {len(requestsNo)}")
print("Invoking the dequeue() method: ")
print(dequeue())
print(f"Current content of requestsNo after dequeue() method: {requestsNo}")

	# e. Using the provided dictionary below, create lists of its keys, values and items and show those lists.

roman_numerals = {'I': 1, 'II': 2, 'III': 3, 'V': 5}

		# e.1. Print the keys of the "roman_numerals" dictionary

		# e.2. Print the values of the "roman_numerals" dictionary

		# e.3. Print the items of the "roman_numerals" dictionary


# 4. Create a git repository named s01 inside your provided GitLab Subfolder.

# 5. Initialize a git repository, stage the files in preparation for commit, create commit with the message Add Activity Code and push changes to the remote repository.